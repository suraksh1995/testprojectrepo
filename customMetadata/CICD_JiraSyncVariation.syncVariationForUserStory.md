<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>syncVariationForUserStory</label>
    <protected>false</protected>
    <values>
        <field>CICD_JiraIssueFieldName__c</field>
        <value xsi:type="xsd:string">fields.project.key</value>
    </values>
    <values>
        <field>CICD_SfFieldApiName__c</field>
        <value xsi:type="xsd:string">copado__Project__r.CICD_JiraProjectKey__c</value>
    </values>
    <values>
        <field>CICD_SfObjectApiName__c</field>
        <value xsi:type="xsd:string">copado__User_Story__c</value>
    </values>
</CustomMetadata>
