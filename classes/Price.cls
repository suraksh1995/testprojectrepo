public class Price {

//This field has an Apex-defined data type in the Amount Apex class.
@AuraEnabled
public Amount amount;


//This field has an Apex-defined data type in the SalesUnit Apex class.
@AuraEnabled
public SalesUnit salesUnit;

}