@isTest
private class SampleApexTest{
static testMethod void validateSampleApex() {
case c = new case(New_Case_field__c='100000');
 
Test.startTest();
insert c;
Test.stopTest();
 
// Retrieve the record
c = [SELECT New_Case_field__c FROM case WHERE Id =:c.Id];
 
// Test that the trigger correctly updated the price
System.assertEquals('90000', c.New_Case_field__c);
}
}